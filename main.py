import numpy as np
from random import *
from copy import deepcopy
import matplotlib.pyplot as plt
from matplotlib import animation
import multiprocessing as mp
from datetime import datetime
import numba

probTree = 0.8  # probability that the site contains a tree
probBurning = 0.01  # probability that the tree is burning
probImmune = 0.3  # probability that the tree is immune
probLightning = 0.001  # probability of a lightning strike

EMPTY = 0  # empty site
NONBURNING = 1  # site with non-burning tree
BURNING = 2  # site with burning tree
BORDER = 3


def initForestGrid(n):
    # create the grid and initialise the values to 3 = Border
    forestGrid = BORDER * np.ones((n+2, n+2))
    for i in range(1, n+1):
        for j in range(1, n+1):
            if random() < probTree:  # there is a tree in a site
                if random() < probBurning:
                    # generate a burning tree
                    forestGrid[i, j] = BURNING
                else:
                    # generate a non-burning tree
                    forestGrid[i, j] = NONBURNING
            else:
                forestGrid[i, j] = EMPTY  # site is empty
    return forestGrid


@numba.jit(nopython=True, parallel=True)
def initForestGridParallel(n):
    # create the grid and initialise the values to 3 = Border
    forestGrid = BORDER * np.ones((n+2, n+2))
    for i in numba.prange(1, n+1):
        for j in numba.prange(1, n+1):
            if random() < probTree:  # there is a tree in a site
                if random() < probBurning:
                    forestGrid[i, j] = BURNING  # generate a burning tree
                else:
                    # generate a non-burning tree
                    forestGrid[i, j] = NONBURNING
            else:
                forestGrid[i, j] = EMPTY  # site is empty
    return forestGrid


def spread(site, neighborhood):
    if site == EMPTY or site == BURNING:
        val = EMPTY
    else:
        if random() < probLightning or BURNING in neighborhood:
            if random() < probImmune:
                val = NONBURNING
            else:
                val = BURNING
        else:
            val = NONBURNING
    return val


def applySpread(forestGridExt):
    ni = forestGridExt.shape[0] - 2
    nj = forestGridExt.shape[1] - 2
    forestGridCopy = deepcopy(forestGridExt)
    for i in range(1, ni+1):
        for j in range(1, nj+1):
            site = forestGridExt[i, j]
            N = forestGridExt[i-1, j]
            NE = forestGridExt[i-1, j+1]
            E = forestGridExt[i, j + 1]
            SE = forestGridExt[i + 1, j + 1]
            S = forestGridExt[i + 1, j]
            SW = forestGridExt[i + 1, j - 1]
            W = forestGridExt[i, j - 1]
            NW = forestGridExt[i - 1, j - 1]
            mooreNeighborhood = [N, NE, E, SE, S, SW, W, NW]
            vonNeumanNeighborhood = [N, E, S, W]
            # spread using moore neighborhood
            forestGridCopy[i, j] = spread(site, vonNeumanNeighborhood)
    return forestGridCopy


@numba.jit(parallel=True, forceobj=True)
def applySpreadParallel(forestGridExt):
    ni = forestGridExt.shape[0] - 2
    nj = forestGridExt.shape[1] - 2
    forestGridCopy = deepcopy(forestGridExt)
    for i in numba.prange(1, ni+1):
        for j in numba.prange(1, nj+1):
            site = forestGridExt[i, j]
            N = forestGridExt[i-1, j]
            NE = forestGridExt[i-1, j+1]
            E = forestGridExt[i, j + 1]
            SE = forestGridExt[i + 1, j + 1]
            S = forestGridExt[i + 1, j]
            SW = forestGridExt[i + 1, j - 1]
            W = forestGridExt[i, j - 1]
            NW = forestGridExt[i - 1, j - 1]
            mooreNeighborhood = [N, NE, E, SE, S, SW, W, NW]
            vonNeumanNeighborhood = [N, E, S, W]
            forestGridCopy[i, j] = spread(site, mooreNeighborhood)
    return forestGridCopy


def forestFireSimulation(n, t):
    start = datetime.now()
    forestGrid = initForestGrid(n)
    forestGrids = []
    forestGrids.append(forestGrid)
    for timeStep in range(t):
        forestGrid = applySpread(forestGrid)
        forestGrids.append(forestGrid)
    end = datetime.now()
    print(f"forestFireSimulation took {end-start}")
    return forestGrids


@numba.jit(parallel=True, forceobj=True)
def forestFireSimulationParallel(n, t):
    start = datetime.now()
    forestGrid = initForestGridParallel(n)
    forestGrids = []
    forestGrids.append(forestGrid)
    for _ in numba.prange(t):
        forestGrid = applySpreadParallel(forestGrid)
        forestGrids.append(forestGrid)
    end = datetime.now()
    print(f"forestFireSimulationParallel took {end-start}")
    return forestGrids


def visualize(forestGrids):
    palette = np.array([[211,   211,   211],   # empty
                        [50, 205,   50],   # tree
                        [255,   69,   0],   # burning
                        [139, 69, 19]])  # border
    fig, ax = plt.subplots()
    ims = []
    for i in range(len(forestGrids)):
        RGB = palette[forestGrids[i].astype(int)]
        im = ax.imshow(RGB, animated=True)
        if i == 0:
            ax.imshow(RGB)  # show an initial one first
        ims.append([im])
    #ax.text(-30, 5, 'BORDER',
    #        bbox={'facecolor': '#8B4513', 'pad': 5})  # border
    #ax.text(-30, 10, 'BURNING TREE', bbox={'facecolor': '#ff4500', 'pad': 5})  # tree
    #ax.text(-30, 15, 'NON-BURNING TREE',
    #        bbox={'facecolor': '#32cd32', 'pad': 5})  # burning
    #ax.text(-30, 20, 'EMPTY',
    #        bbox={'facecolor': '#d3d3d3', 'pad': 5})  # empty
    ani = animation.ArtistAnimation(
        fig, ims,  interval=100, blit=True, repeat=True)
    plt.show()


if __name__ == '__main__':
    forestGrids = forestFireSimulation(800, 100)
    visualize(forestGrids)
